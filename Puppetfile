################
## SIMP 6.2.0 ##
################


mod 'camptocamp-kmod',
  :git => 'https://github.com/simp/puppet-kmod.git',
  :tag => '2.2.0'

# We are maintaining a simp-master branch of this module
# because camptocamp has moved to Hiera 5 and Puppet >=
# 4.10.10.  We need to release our version of 1.1.1
# which is on simp-master.
mod 'simp-systemd',
  :git => 'https://github.com/simp/puppet-systemd.git',
  :tag => 'simp-1.1.1'

mod 'cristifalcas-journald',
  :git => 'https://github.com/simp/pupmod-simp-journald.git',
  :tag => '0.6.0'

mod 'elastic-elasticsearch',
  :git => 'https://github.com/simp/puppet-elasticsearch',
  :tag => '5.5.0'

mod 'elastic-logstash',
  :git => 'https://github.com/simp/puppet-logstash',
  :tag => '5.3.0'

mod 'herculesteam-augeasproviders_apache',
  :git => 'https://github.com/simp/augeasproviders_apache',
  :tag => '3.0.0'

mod 'herculesteam-augeasproviders_core',
  :git => 'https://github.com/simp/augeasproviders_core',
  :tag => '2.1.4'

mod 'herculesteam-augeasproviders_grub',
  :git => 'https://github.com/simp/augeasproviders_grub',
  :tag => '3.0.1'

mod 'herculesteam-augeasproviders_mounttab',
  :git => 'https://github.com/simp/augeasproviders_mounttab',
  :tag => '2.0.3'

mod 'herculesteam-augeasproviders_nagios',
  :git => 'https://github.com/simp/augeasproviders_nagios',
  :tag => '2.0.2'

mod 'herculesteam-augeasproviders_pam',
  :git => 'https://github.com/simp/augeasproviders_pam',
  :tag => '2.1.1'

mod 'herculesteam-augeasproviders_postgresql',
  :git => 'https://github.com/simp/augeasproviders_postgresql',
  :tag => '3.0.0'

mod 'herculesteam-augeasproviders_puppet',
  :git => 'https://github.com/simp/augeasproviders_puppet',
  :tag => '2.1.1'

mod 'herculesteam-augeasproviders_shellvar',
  :git => 'https://github.com/simp/augeasproviders_shellvar',
  :tag => '3.0.0'

mod 'herculesteam-augeasproviders_ssh',
  :git => 'https://github.com/simp/augeasproviders_ssh',
  :tag => '3.0.0'

mod 'herculesteam-augeasproviders_sysctl',
  :git => 'https://github.com/simp/augeasproviders_sysctl',
  :tag => '2.2.0'

mod 'onyxpoint-gpasswd',
  :git => 'https://github.com/simp/puppet-gpasswd',
  :tag => '1.0.5'

mod 'puppet-grafana',
  :git => 'https://github.com/simp/puppet-grafana.git',
  :tag => 'v4.1.1'

mod 'puppetlabs-apache',
  :git => 'https://github.com/simp/puppetlabs-apache',
  :tag => '3.0.0'

mod 'puppetlabs-concat',
  :git => 'https://github.com/simp/puppetlabs-concat',
  :tag => '4.1.1'

mod 'puppetlabs-docker',
  :git => 'https://github.com/simp/puppetlabs-docker',
  :tag => '1.1.0'

mod 'puppetlabs-hocon',
  :git => 'https://github.com/simp/pupmod-puppetlabs-hocon',
  :tag => '1.0.0'

mod 'puppetlabs-inifile',
  :git => 'https://github.com/simp/puppetlabs-inifile',
  :tag => '2.2.0'

mod 'puppetlabs-java',
  :git => 'https://github.com/simp/puppetlabs-java',
  :tag => '2.4.0'

mod 'puppetlabs-motd',
  :git => 'https://github.com/simp/puppetlabs-motd',
  :tag => '1.9.0'

mod 'puppetlabs-mount_providers',
  :git => 'https://github.com/simp/puppetlabs-mount_providers',
  :tag => '1.0.0'

mod 'puppetlabs-mysql',
  :git => 'https://github.com/simp/puppetlabs-mysql',
  :tag => '5.3.0'

mod 'puppetlabs-postgresql',
  :git => 'https://github.com/simp/puppetlabs-postgresql',
  :tag => '5.4.0'

mod 'puppetlabs-puppetdb',
  :git => 'https://github.com/simp/puppetlabs-puppetdb',
  :tag => '6.0.2'

mod 'puppetlabs-puppet_authorization',
  :git => 'https://github.com/simp/puppetlabs-puppet_authorization.git',
  :tag => '0.4.0'

mod 'puppetlabs-stdlib',
  :git => 'https://github.com/simp/puppetlabs-stdlib',
  :tag => '4.25.1'

mod 'puppetlabs-translate',
  # FIXME LATER:  Can't use latest version, because puppetlabs-docker 1.0.5
  # requires puppetlabs-translate < 1.1.0.
  :git => 'https://github.com/simp/pupmod-puppetlabs-translate',
  :tag => '1.0.0'

mod 'razorsedge-snmp',
  :git => 'https://github.com/simp/puppet-snmp',
  :tag => '3.9.0'

mod 'richardc-datacat',
  # FIXME Next datacat release.
  # This is a bit messy.  This project has 3 tags that are
  # all version 0.6.2, but slightly different:
  #   '0.6.2 from the owner
  #   'simp-0.6.2' from SIMP team for SIMP4/SIMP5
  #   'simp6.0.0-0.6.2' from SIMP team for SIMP6
  # We are going to use the last SIMP team release, until
  # datacat gets updated and we can get out
  # of this mess.
  :git => 'https://github.com/simp/puppet-datacat',
  :tag => 'simp6.0.0-0.6.2'

mod 'simp-acpid',
  :git => 'https://github.com/simp/pupmod-simp-acpid',
  :branch => 'master'

mod 'simp-aide',
  :git => 'https://github.com/simp/pupmod-simp-aide',
  :branch => 'master'

mod 'simp-at',
  :git => 'https://github.com/simp/pupmod-simp-at',
  :branch => 'master'

mod 'simp-auditd',
  :git => 'https://github.com/simp/pupmod-simp-auditd',
  :branch => 'master'

mod 'simp-autofs',
  :git => 'https://github.com/simp/pupmod-simp-autofs',
  :branch => 'master'

mod 'simp-chkrootkit',
  :git => 'https://github.com/simp/pupmod-simp-chkrootkit',
  :branch => 'master'

mod 'simp-clamav',
  :git => 'https://github.com/simp/pupmod-simp-clamav',
  :branch => 'master'

mod 'simp-compliance_markup',
  :git => 'https://github.com/simp/pupmod-simp-compliance_markup',
  :branch => 'master'

mod 'simp-cron',
  :git => 'https://github.com/simp/pupmod-simp-cron',
  :branch => 'master'

mod 'simp-dconf',
  :git => 'https://github.com/simp/pupmod-simp-dconf',
  :tag => '0.0.1'

mod 'simp-dhcp',
  :git => 'https://github.com/simp/pupmod-simp-dhcp',
  :branch => 'master'

mod 'simp-dirtycow',
  :git => 'https://github.com/simp/pupmod-simp-dirtycow.git',
  :branch => 'master'

mod 'simp-fips',
  :git => 'https://github.com/simp/pupmod-simp-fips',
  :branch => 'master'

mod 'simp-freeradius',
  :git => 'https://github.com/simp/pupmod-simp-freeradius',
  :branch => 'master'

mod 'simp-gdm',
  :git => 'https://github.com/simp/pupmod-simp-gdm',
  :branch => 'master'

mod 'simp-gnome',
  :git => 'https://github.com/simp/pupmod-simp-gnome',
  :branch => 'master'

# SIMP has made changes to this module that don't exist upstream
# and owns this version.  Beginning with version 0.4.4, all module
# changes will be done on the 'master' branch.
mod 'simp-haveged',
  :git => 'https://github.com/simp/pupmod-simp-haveged',
  :branch => 'master'

mod 'simp-incron',
  :git => 'https://github.com/simp/pupmod-simp-incron',
  :branch => 'master'

mod 'simp-iptables',
  :git => 'https://github.com/simp/pupmod-simp-iptables',
  :branch => 'master'

mod 'simp-issue',
  :git => 'https://github.com/simp/pupmod-simp-issue',
  :branch => 'master'

mod 'simp-krb5',
  :git => 'https://github.com/simp/pupmod-simp-krb5',
  :branch => 'master'

mod 'simp-libreswan',
  :git => 'https://github.com/simp/pupmod-simp-libreswan',
  :branch => 'master'

mod 'simp-libvirt',
  :git => 'https://github.com/simp/pupmod-simp-libvirt',
  :branch => 'master'

mod 'simp-logrotate',
  :git => 'https://github.com/simp/pupmod-simp-logrotate',
  :branch => 'master'

mod 'simp-mozilla',
  :git => 'https://github.com/simp/pupmod-simp-mozilla',
  :branch => 'master'

mod 'simp-named',
  :git => 'https://github.com/simp/pupmod-simp-named',
  :branch => 'master'

mod 'simp-network',
  :git => 'https://github.com/simp/pupmod-simp-network',
  :branch => 'master'

mod 'simp-nfs',
  :git => 'https://github.com/simp/pupmod-simp-nfs',
  :branch => 'master'

mod 'simp-ntpd',
  :git => 'https://github.com/simp/pupmod-simp-ntpd',
  :branch => 'master'

mod 'simp-oddjob',
  :git => 'https://github.com/simp/pupmod-simp-oddjob',
  :branch => 'master'

mod 'simp-openscap',
  :git => 'https://github.com/simp/pupmod-simp-openscap',
  :branch => 'master'

mod 'simp-pam',
  :git => 'https://github.com/simp/pupmod-simp-pam',
  :branch => 'master'

mod 'simp-pki',
  :git => 'https://github.com/simp/pupmod-simp-pki',
  :branch => 'master'

mod 'simp-polkit',
  :git => 'https://github.com/simp/pupmod-simp-polkit',
  :branch => 'master'

mod 'simp-postfix',
  :git => 'https://github.com/simp/pupmod-simp-postfix',
  :branch => 'master'

mod 'simp-pupmod',
  :git => 'https://github.com/simp/pupmod-simp-pupmod',
  :branch => 'master'

mod 'simp-resolv',
  :git => 'https://github.com/simp/pupmod-simp-resolv',
  :branch => 'master'

mod 'simp-rsync',
  :git => 'https://github.com/simp/pupmod-simp-rsync',
  :branch => 'master'

mod 'simp-rsyslog',
  :git => 'https://github.com/simp/pupmod-simp-rsyslog',
  :branch => 'master'

mod 'simp-selinux',
  :git => 'https://github.com/simp/pupmod-simp-selinux',
  :branch => 'master'

mod 'simp-simp',
  :git => 'https://github.com/jeefberkey/pupmod-simp-simp',
  :branch => 'SIMP-4009'

mod 'simp-simpcat',
  :git => 'https://github.com/simp/pupmod-simp-simpcat',
  :branch => 'master'

mod 'simp-simplib',
  :git => 'https://github.com/simp/pupmod-simp-simplib',
  :branch => 'master'

mod 'simp-simp_apache',
  :git => 'https://github.com/simp/pupmod-simp-simp_apache',
  :branch => 'master'

mod 'simp-simp_banners',
  :git => 'https://github.com/simp/pupmod-simp-simp_banners',
  :branch => 'master'

mod 'simp-simp_docker',
  :git => 'https://github.com/simp/pupmod-simp-simp_docker',
  :branch => 'master'

mod 'simp-simp_elasticsearch',
  :git => 'https://github.com/simp/pupmod-simp-simp_elasticsearch',
  :branch => 'master'

mod 'simp-simp_gitlab',
  :git => 'https://github.com/simp/pupmod-simp-simp_gitlab',
  :branch => 'master'

mod 'simp-simp_grafana',
  :git => 'https://github.com/simp/pupmod-simp-simp_grafana',
  :branch => 'master'

mod 'simp-simp_ipa',
  :git => 'https://github.com/simp/pupmod-simp-simp_ipa',
  :branch => 'master'

mod 'simp-simp_logstash',
  :git => 'https://github.com/simp/pupmod-simp-simp_logstash',
  :branch => 'master'

mod 'simp-simp_nfs',
  :git => 'https://github.com/simp/pupmod-simp-simp_nfs',
  :branch => 'master'

mod 'simp-simp_openldap',
  :git => 'https://github.com/simp/pupmod-simp-simp_openldap',
  :branch => 'master'

mod 'simp-simp_options',
  :git => 'https://github.com/simp/pupmod-simp-simp_options',
  :branch => 'master'

mod 'simp-simp_rsyslog',
  :git => 'https://github.com/simp/pupmod-simp-simp_rsyslog',
  :branch => 'master'

mod 'simp-simp_snmpd',
  :git => 'https://github.com/simp/pupmod-simp-simp_snmpd',
  :branch => 'master'

mod 'simp-vox_selinux',
  :git => 'https://github.com/simp/pupmod-voxpupuli-selinux',
  :branch => 'simp-master'

mod 'simp-site',
  :git => 'https://github.com/simp/pupmod-simp-site',
  :branch => 'master'

mod 'simp-ssh',
  :git => 'https://github.com/simp/pupmod-simp-ssh',
  :branch => 'master'

mod 'simp-sssd',
  :git => 'https://github.com/simp/pupmod-simp-sssd',
  :branch => 'master'

mod 'simp-stunnel',
  :git => 'https://github.com/simp/pupmod-simp-stunnel',
  :branch => 'master'

mod 'simp-sudo',
  :git => 'https://github.com/simp/pupmod-simp-sudo',
  :branch => 'master'

mod 'simp-sudosh',
  :git => 'https://github.com/simp/pupmod-simp-sudosh',
  :branch => 'master'

mod 'simp-svckill',
  :git => 'https://github.com/simp/pupmod-simp-svckill',
  :branch => 'master'

mod 'simp-swap',
  :git => 'https://github.com/simp/pupmod-simp-swap',
  :branch => 'master'

mod 'simp-tcpwrappers',
  :git => 'https://github.com/simp/pupmod-simp-tcpwrappers',
  :branch => 'master'

mod 'simp-tftpboot',
  :git => 'https://github.com/simp/pupmod-simp-tftpboot',
  :branch => 'master'

# SIMP has made changes to this module that don't exist upstream
# yet. (PR pending). Since may revert to saz/puppet-timezone at a
# later date, working off of simp-master.
mod 'simp-timezone',
  :git => 'https://github.com/simp/pupmod-simp-timezone',
  :branch => 'master'

mod 'simp-tpm',
# FUTURE: Release 2.0.0 (version on master)
# This functionality requires RedHat releases to tpm2-tools and tpm2-tss.
#
# This currently exists on a 1.2.X branch for CHANGELOG fixes
  :git => 'https://github.com/simp/pupmod-simp-tpm',
  :branch => 'master'

mod 'simp-tuned',
  :git => 'https://github.com/simp/pupmod-simp-tuned',
  :branch => 'master'

mod 'simp-upstart',
  :git => 'https://github.com/simp/pupmod-simp-upstart',
  :branch => 'master'

mod 'simp-useradd',
  :git => 'https://github.com/simp/pupmod-simp-useradd',
  :branch => 'master'

mod 'simp-vnc',
  :git => 'https://github.com/simp/pupmod-simp-vnc',
  :branch => 'master'

mod 'simp-vsftpd',
  :git => 'https://github.com/simp/pupmod-simp-vsftpd',
  :branch => 'master'

mod 'simp-xinetd',
  :git => 'https://github.com/simp/pupmod-simp-xinetd',
  :branch => 'master'

# FIXME: The code we have already packaged and published as an RPM
# has not been released by trlinkin. Our RPM-packaged version contains
# 1 non-essential commit beyond the 2.0.0 tag. Until trlinkin releases
# a later version of this module, for RPM-generation, we are stuck with
# this odd version only accessible via a git ref spec. For non-RPM
# SIMP installs, however, the '2.0.0' version available on PuppetForge
# will work just fine.
mod 'trlinkin-nsswitch',
  :git => 'https://github.com/simp/puppet-nsswitch',
  # :tag => 'simp6.0.0-2.0.0'
  :branch => 'master'


mod 'voxpupuli-yum',
  :git => 'https://github.com/voxpupuli/puppet-yum',
  :branch => 'master'

mod 'vshn-gitlab',
  :git => 'https://github.com/simp/puppet-gitlab.git',
  :branch => 'master'

mod 'puppetlabs-reboot', '2.0.0'

# vi:syntax=ruby
