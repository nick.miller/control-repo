# vim: set expandtab:
# Workarounds for Puppet Enterprise
#
# @param console_systems The systems that are allowed to access the PE console
# @param client_nets The client systems that are allowed to connect to PE.
# @param enable_iptables Provide support for the SIMP IPTables stack
#
class profiles::pe_master (
  Simplib::Netlist $console_systems = ['127.0.0.1'],
  Boolean $firewall = simplib::lookup('simp_options::firewall', { 'default_value' => true }),
  Simplib::Netlist $trusted_nets = simplib::lookup('simp_options::trusted_nets', { 'default_value' => ['127.0.0.1','::1'] }),
) {

  if $firewall {
    include '::iptables'

    iptables::listen::tcp_stateful { 'allow_pe_console':
      dports => [443],
    }

    iptables::listen::tcp_stateful { 'allow_pe_clients':
      dports => [8140,61613,8142],
    }
  }

  pe_ini_setting { 'digest_algorithm':
    ensure  => present,
    setting => 'digest_algorithm',
    value   => 'sha256',
    section => 'main',
    path    => '/etc/puppetlabs/puppet/puppet.conf',
  }
}
