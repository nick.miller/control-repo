# Add auth rules using pupmod::master::simp_auth
#
class profiles::pe_simp_auth {
  class { 'pupmod::master::simp_auth':
    server_distribution => 'PE'
  }
  service { 'pe-puppetserver': }
}
